app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider' , function($stateProvider, $urlRouterProvider, $locationProvider){
   $urlRouterProvider.otherwise("/home");
   //$locationProvider.html5Mode(true);
   $stateProvider
       .state("home", {
       url:"/home",
       controller: "mainCtrl",
       templateUrl:"template/main.html"
   })
}])
    